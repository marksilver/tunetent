# Tunetent

[Bandcamp](https://bandcamp.com) is a nice utility, but it's not without its flaws. First of all, go visit any artist's bandcamp page with the [uMatrix browser extension](https://addons.mozilla.org/en-US/firefox/addon/umatrix/) installed. There is no reason for a music streaming website to connect you to facebook, google, et cetera. But that's not where bandcamp's inefficiency ends. There is no reason to write a proprietary HTML5-based mp3 streamer when your mp3s are DRM-free in the first place. You can replicate the same functionality with the native HTML audio tags & a bit of javascript, which take less time & resources to load than what bandcamp offers.

Tunetent is my solution to these problems that Bandcamp creates. But really, this is just a repo of a few web pages & scripts that prove how easy it is to host a bandcamp-like website for yourself. If you have a very basic understanding of HTML, you can easily fork this repo and adapt it for your own music. Keep it simple, stupid.

## Licensing

First and foremost, I want to make it clear that if you fork this project, you can do pretty much whatever you want with it. Any audio you share with the world using this project is yours to license as you please. I want to make it clear that this project is licensed under version 2 of the GNU GPL, meaning that you should keep open the source code of your fork. I won't sue you if you don't, of course, because the framework I provide is just a bunch of stuff you could look up for yourself. I just think it would be cool, in the event that you add functional changes to your fork of this repo, to be able to see how your changes operate & *possibly* adapt them upstream. The license here is symbolic because freedom is cool, man!
